package com.example.eddasgenomgang.Game;

import com.example.eddasgenomgang.Game.GameInterface.GameStatus;
import com.example.eddasgenomgang.Game.GameInterface.Move;
import com.example.eddasgenomgang.Player.PlayerEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Table
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class GameEntity {
    @Id
    @GeneratedValue //test
    UUID id;

    GameStatus gameStatus = GameStatus.OPEN; //set game status on default as open
    Move playerOneMove;
    Move playerTwoMove;


    @OneToOne
    PlayerEntity playerOne;

    @OneToOne
    PlayerEntity playerTwo;


    public UUID getId() {
        return id;
    }

    public GameStatus getStatus() {
        return gameStatus;
    }
}
