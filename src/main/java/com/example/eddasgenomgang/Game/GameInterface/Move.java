package com.example.eddasgenomgang.Game.GameInterface;


public enum Move {
/*
    @Enumerated(EnumType.STRING)
*/
    ROCK{        @Override
public boolean isBetterThan(Move move) {
    return (SCISSORS.equals(move));
}},
    PAPER {
        @Override
        public boolean isBetterThan(Move move) {
            return (ROCK.equals(move));
        }
    },
    SCISSORS {
        @Override
        public boolean isBetterThan(Move move) {
            return (PAPER.equals(move));
        }
    };
    public abstract boolean isBetterThan(Move move);
}


