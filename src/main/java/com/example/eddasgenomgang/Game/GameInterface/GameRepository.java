package com.example.eddasgenomgang.Game.GameInterface;

import com.example.eddasgenomgang.Game.GameEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.*;
@Repository
public interface GameRepository extends JpaRepository<GameEntity, UUID> {

}

