package com.example.eddasgenomgang.Game.GameInterface;
import com.example.eddasgenomgang.Game.GameEntity;
import com.example.eddasgenomgang.Player.PlayerEntity;
import com.example.eddasgenomgang.Player.PlayerInterface.PlayerService;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class GameService {
    final
    GameRepository gameRepository;
    final
    PlayerService playerService;

    public GameService(GameRepository gameRepository, PlayerService playerService) {
        this.gameRepository = gameRepository;
        this.playerService = playerService;
    }

    public GameStatusDTO entityToDTO(GameEntity gameEntity, PlayerEntity whoIsPlayer) {

        PlayerEntity opponent;
        Move playerMove;
        Move opponentMove;
        if (whoIsPlayer.equals(gameEntity.getPlayerOne())) {// playerONe is the player who sends request, so opponent will be playerTwo
            opponent = gameEntity.getPlayerTwo();
            opponentMove = gameEntity.getPlayerTwoMove();
            playerMove = gameEntity.getPlayerOneMove();
        } else {//playerTwo is theplayer who sends request, and playerOne is the opponent

            opponent = gameEntity.getPlayerOne();
            opponentMove = gameEntity.getPlayerOneMove();
            playerMove = gameEntity.getPlayerTwoMove();
        }
        String playerName = (whoIsPlayer != null) ? whoIsPlayer.getName() : null;
        String opponentName = (opponent != null) ? opponent.getName() : null;
        return new GameStatusDTO(gameEntity.getId(),
                playerName,
                playerMove,
                gameEntity.getGameStatus(),
                opponentName,
                opponentMove);
    }

    public GameStatusDTO joinGame(UUID gameId, UUID playerTwoID) {//methos that returns game from repository,finds it through UUID
        GameEntity game = gameRepository.findById(gameId).orElseThrow(); //just like in player service method findByID returns OPtional, that's why in case not finding it throws exception.
        GameStatus gameStatus = game.getGameStatus();
        if (gameStatus == GameStatus.OPEN) {
            PlayerEntity playerTwo = playerService.getPlayer(playerTwoID);
            game.setPlayerTwo(playerTwo);
            game.setGameStatus(GameStatus.ACTIVE);
            gameRepository.save(game);
            playerTwo.setGame(game);
            playerService.savePlayer(playerTwo);
            return new GameStatusDTO(game.getId(), playerTwo.getName(), null, game.getGameStatus(), game.getPlayerOne().getName(), null);

        } else {
            throw new IllegalArgumentException();
        }

    }


    public GameStatusDTO createGame(UUID playerId) {

        GameEntity game = new GameEntity();//first game entity gets created and gives game id
        PlayerEntity playerOne = playerService.getPlayer(playerId); //takes id of Player who created it
        game.setPlayerOne(playerOne);    // and sets it as player one
        gameRepository.save(game);
        playerOne.setGame(game);
        playerService.savePlayer(playerOne);//changed this line to saving player in repository through service, not directly
        return new GameStatusDTO(game.getId(), playerOne.getName(), null, game.getGameStatus(), null, null);
    }

    public List<GameStatusDTO> showGames() {
        return gameRepository.findAll().stream()
                .filter(gameEntity -> gameEntity.getGameStatus() == GameStatus.OPEN)
                .map(gameEntity -> {
                    String name = (gameEntity.getPlayerOne() != null) ? gameEntity.getPlayerOne().getName() : null;
                    String name2 = (gameEntity.getPlayerTwo() != null) ? gameEntity.getPlayerTwo().getName() : null;
                    return new GameStatusDTO(gameEntity.getId(),
                            name,
                            null,
                            gameEntity.getGameStatus(),
                            name2,
                            null);
                })
                .toList();

    }

    public GameStatusDTO gameInfo(UUID gameId, UUID playerId) {
        GameEntity game = gameRepository.findById(gameId).orElseThrow();
        PlayerEntity player = playerService.getPlayer(playerId);
        return entityToDTO(game,player);
    }

    public GameStatusDTO makeMove(UUID playerId, Move move) {
        PlayerEntity player = playerService.getPlayer(playerId);
        GameEntity game = player.getGame();
        if (game == null) {
            throw new NoSuchElementException();
        }
        if (player.equals(game.getPlayerOne())) {
            game.setPlayerOneMove(move);
        } else {
            game.setPlayerTwoMove(move);
        }
        gameRepository.save(game);
        return entityToDTO(game,player);
    }

}
