package com.example.eddasgenomgang.Game.GameInterface;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import java.util.UUID;

@Jacksonized
@Builder
@Value
public class GameStatusDTO {
    UUID id;
    String name;
    Move move;
    GameStatus gameStatus;
    String opponentName;
    Move opponentMove;
}

