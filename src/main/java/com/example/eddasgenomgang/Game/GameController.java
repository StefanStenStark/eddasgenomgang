package com.example.eddasgenomgang.Game;


import com.example.eddasgenomgang.Game.GameInterface.GameService;
import com.example.eddasgenomgang.Game.GameInterface.GameStatusDTO;
import com.example.eddasgenomgang.Game.GameInterface.Move;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(path = "/rock-paper-scissors/", produces = "application/json")

public class GameController {
    final
    GameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @PostMapping("/games/start")
    public GameStatusDTO startGame(@RequestHeader Map<String, String> headers) {
        String id = headers.get("token");
        GameStatusDTO game = gameService.createGame(UUID.fromString(id));
        return game;

    }

    @GetMapping("/games/join/{gameId}")
    public GameStatusDTO joinGame(@PathVariable UUID gameId,@RequestHeader Map<String, String> headers) {//method that returns game and takes UUID. basically
        String playerTwoId = headers.get("token");
        GameStatusDTO game = gameService.joinGame(gameId,UUID.fromString(playerTwoId));
        return game;
    }

    @GetMapping ("/games")
    public List<GameStatusDTO> showGames(){
        return gameService.showGames();
     }

    @GetMapping ("/games/{gameId}")
    public GameStatusDTO gameInfo(@PathVariable UUID gameId,@RequestHeader("token") UUID playerId){
        return gameService.gameInfo(gameId,playerId);
    }

    @PostMapping ("/games/move/{sign}")
    public GameStatusDTO makeMove(@PathVariable Move sign, @RequestHeader("token") UUID playerId){
        return gameService.makeMove(playerId,sign);
    }
}
