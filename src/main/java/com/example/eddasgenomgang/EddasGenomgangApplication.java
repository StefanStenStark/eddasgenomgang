package com.example.eddasgenomgang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class EddasGenomgangApplication {

    public static void main(String[] args) {
        SpringApplication.run(EddasGenomgangApplication.class, args);
    }

}
