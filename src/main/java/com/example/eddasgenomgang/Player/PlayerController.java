package com.example.eddasgenomgang.Player;

import com.example.eddasgenomgang.Player.PlayerInterface.PlayerService;
import com.example.eddasgenomgang.Player.PlayerInterface.SetNameRequestDTO;
import org.springframework.web.bind.annotation.*;
import java.util.*;


@RestController
@CrossOrigin
@RequestMapping("/rock-paper-scissors")
public class PlayerController {
    final
    PlayerService playerService;

    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    @GetMapping("/auth/token")
    public UUID getPlayerToken() {   //method to get UUID and store it in Player entity player by calling method createPlayer in playerService
        PlayerEntity player = playerService.createPlayer();
        return player.getId();
    }

    @PostMapping("/user/name")
    public void setName(@RequestHeader Map<String, String> headers, @RequestBody SetNameRequestDTO request) { //method to set name of the player that takes UUID as a token
        var id = headers.get("token");// and a name in the request body

        playerService.setName(UUID.fromString(id), request.getName());  // i pass a string in UUID format, and by .fromString turn it in to UUID object

    }
}
