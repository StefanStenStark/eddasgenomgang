package com.example.eddasgenomgang.Player.PlayerInterface;


import com.example.eddasgenomgang.Player.PlayerEntity;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class PlayerService {

    final
    PlayerRepository playerRepository;

    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public PlayerEntity createPlayer() {// creates playerEntity with generated UUID and saves it in Repository
        PlayerEntity player = new PlayerEntity();
        playerRepository.save(player);
        return player;
    }

    public void setName(UUID id, String name) {//
        PlayerEntity playerUpdateName = playerRepository.findById(id).orElseThrow();//findByID is a standard method in repository that returns Type <> Optional in our case optional of playerEntity
        playerUpdateName.setName(name);                                             //passing the name into playerUpdateName into the excisting player that had token
        playerRepository.saveAndFlush(playerUpdateName);//saveAndFlush is a method that commits to the database immediately


    }

    public PlayerEntity getPlayer(UUID id) { //method that finds player in repository and returns it
        PlayerEntity playerEntity = playerRepository.findById(id).orElseThrow();
        return playerEntity;

    }

    public void savePlayer(PlayerEntity player) {
        playerRepository.save(player);


    }

}
