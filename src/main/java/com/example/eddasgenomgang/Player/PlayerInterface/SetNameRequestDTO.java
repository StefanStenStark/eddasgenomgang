package com.example.eddasgenomgang.Player.PlayerInterface;

import lombok.Builder;

import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Jacksonized
@Value
@Builder

public class SetNameRequestDTO {

    String name;


}
