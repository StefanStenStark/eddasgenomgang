package com.example.eddasgenomgang.Player.PlayerInterface;

import com.example.eddasgenomgang.Player.PlayerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.*;

@Repository
public interface PlayerRepository extends JpaRepository<PlayerEntity, UUID> {
}
