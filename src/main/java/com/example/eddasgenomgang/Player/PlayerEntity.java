package com.example.eddasgenomgang.Player;

import com.example.eddasgenomgang.Game.GameEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.*;


@Entity

@Table
@AllArgsConstructor
@NoArgsConstructor
@Data

public class PlayerEntity {
    @Id
    @GeneratedValue
    private UUID id;
    private String name;

    @OneToOne
    private GameEntity game;
}
